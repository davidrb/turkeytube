const port = 3000;

const express = require('express');
const session = require('express-session');
const flash = require('express-flash');
const bodyParser = require('body-parser');
const hbs = require('hbs');

const app = express();

const FlexSearch = require('flexsearch')

let index = new FlexSearch();

const quotes = [
  'Too much of anything is bad, but too much good whiskey is barely enough.',
  'There is no bad whiskey. There are only some whiskeys that aren\'t as good as others.',
  'My own experience has been that the tools I need for my trade are paper, tobacco, food, and a little whisky.',
  'Never cry over spilt milk. It could’ve been whiskey.',
  'Whiskey, like a beautiful woman, demands appreciation. You gaze first, then it’s time to drink.',
  'The water was not fit to drink. To make it palatable, we had to add whisky. By diligent effort, I learned to like it.',
  'I wish to live to 150 years old, but the day I die, I wish it to be with a cigarette in one hand and a glass of whiskey in the other.',
  'What whiskey will not cure, there is no cure for.',
  'Always carry a flagon of whisky in case of snakebite, and furthermore, always carry a small snake.',
  'I’m on a whisky diet. I’ve lost three days already.',
  'I should never have switched from Scotch to Martinis.',
  'Happiness is having a rare steak, a bottle of whisky, and a dog to eat the rare steak.',
  'I’m a simple man. All I want is enough sleep for two normal men, enough whiskey for three, and enough women for four.',
  'Tell me what brand of whiskey that Grant drinks. I would like to send a barrel of it to my other generals.',
  'Ninety percent I’ll spend on good times, women, and Irish Whiskey. The other ten percent I’ll probably waste.',
  'A good gulp of hot whiskey at bedtime—it’s not very scientific, but it helps.',
  'Love makes the world go round? Not at all. Whiskey makes it go round twice as fast.',
  'Americans are big boys. You can talk them into almost anything. Just sit with them for half an hour over a bottle of whiskey and be a nice guy',
  'Whisky is liquid sunshine.',
  'I have never in my life seen a Kentuckian who didn’t have a gun, a pack of cards, and a jug of whiskey.',
  'The light music of whiskey falling into a glass—an agreeable interlude.',
  'My God, so much I like to drink Scotch that sometimes I think my name is Igor Stra-whiskey.'
]

app.use((req, res, next) => {
  const quote = quotes[Math.floor(Math.random()*quotes.length)];
  res.locals.whiskyQuote = quote;
  next();
})

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.set('view engine', 'hbs')
app.set('view options', { layout: 'layout' })

app.use(express.static('static'))
app.use(express.static('./media'))

app.use(session({
  secure: false,  // TODO: make secure
  secret: 'asdfjkl;qweruiopjdslf',
  cookie: {},
  resave: false,
  saveUninitialized: false
}))

app.use(flash())

app.use(async (req, res, next) => {
  if (req.session.userId) {
    const { username, id } = await db.get(
      `select id, username, email
              from users
              where id=?`,
      [req.session.userId]
    )

    res.locals.user = { username, id };
    req.userInfo = { id, username };
  }

  next();
})

app.get('/', async (req, res, next) => {
  try {
    const videos = await db.all(`
      select title, videos.id, username, uploader, uploadDate
        from videos join users
        on videos.uploader = users.id
        order by uploadDate desc;`);

    res.render('index', { videos })

  } catch (err) {
    next(err);
  }
})

function searchVideos(query) {
  const results = index.search({ query, suggest: true })

  return db.all(`
    select * from videos
      where id in (${results.map(() => '?').join(',')});`, results);
}

app.get('/search', async (req, res) => {
  const { query } = req.query;
  const videos = await searchVideos(query);

  res.render('search', { videos });
})

let db = require('sqlite');

const uuid = require('uuid/v4')

app.get('/signin', (req, res) => {
  if (req.session.userId) {
    req.session.userId = null;
    res.redirect('/signin');
  } else {
    res.render('signin', {
      title: 'sign in'
    })
  }
})

const bcrypt = require('bcrypt')

app.post('/signin', async (req, res, next) => {
  try {
    const { email, password } = req.body;

    const user = await db.get('select * from users where email=?', [email]);

    if (user && await bcrypt.compare(password, user.passwordHash)) {
      req.session.userId = user.id;
      res.redirect('/')
    } else {
      req.flash('error', 'login failed');
      res.redirect('/signin');
    }

  } catch(err) {
    next(err);
  }
})

app.get('/signout', (req, res) => {
  req.session.userId = null;
  res.redirect('/');
})

app.get('/signup', (req, res) => {
  if (req.session.userId) {
    req.session.userId = null;
    res.redirect('/signup');
  } else {
    res.render('signup')
  }
})

app.post('/signup', async (req, res, next) => {
  try {
    const { username, email, password, confirmPassword } = req.body;

    let fail = true;
    if (!username || username === "") {
      req.flash('error', 'username required')

    } else if (!email || email === "") {
      req.flash('error', 'email required')

    } else if (!password || password === "") {
      req.flash('error', 'password required')

    } else if (!confirmPassword || confirmPassword === "") {
      req.flash('error', 'password required')

    } else if (confirmPassword !== password) {
      req.flash('error', 'passwords do not match')

    } else {
      fail = false;
    }

    if (!fail) {
      const id = uuid();

      let passwordHash = await bcrypt.hash(password, 10);

      await db.run(
        'insert into users (id, username, email, passwordHash) values (?, ?, ?, ?)',
        [id, username, email, passwordHash]
      )

      req.session.userId = id;
      res.redirect('/')

    } else {
      res.redirect('/signup')
    }

  } catch(err) {
    next(err)
  }
})

const multer = require('multer')
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './media/videos')
    },
    filename: function (req, file, cb) {
      file.videoId = uuid()
      cb(null, file.videoId + ".mp4")
  }
})

const upload = multer({ storage: storage })
const { exec } = require('child_process');

app.get('/upload', (req, res) => res.render('upload', {
  title: 'upload'
}))

app.post('/upload', upload.single('video'), async (req, res, next) => {
  if (req.session.userId) {
    try {
      const id = req.file.videoId;
      const { title } = req.body;
      const uploader = req.session.userId;
      const uploadDate = new Date();

      exec(`ffmpeg -ss 00:00:00 -i ${req.file.path} -vframes 1 -q:v 2 ./media/thumbnails/${id}.jpg`, async (err, stdout, stderr) => {
        if (err) {
          next(err);

        } else {
          await db.run(
            `insert into videos (id, title, uploader, uploadDate) values (?, ?, ?, ?);`,
            [id, title, uploader, uploadDate.toISOString()]);

          res.redirect('/watch/' + id)
        }
        index.add(id, title);
      });

    } catch(err) {
      next(err)
    }
  } else {
    res.redirect('/signin');
  }
})

app.get('/watch/:videoId', async (req, res, next) => {
  try {
    const { videoId } = req.params;

    const { title, username, uploadDate, uploader } = await db.get(`
      select title, username, uploadDate, uploader
        from users join videos
        on users.id = videos.uploader
        where videos.id = ?;`,
      [videoId])

    const related = await searchVideos(title);

    const comments = await db.all(`
      select * from comments join users
        on users.id = comments.user
        where comments.video = ?
    `, [videoId])

    console.log(related)

    res.render('watch', {
      title: "Watching " + title,
      videoTitle: title,
      videoUrl: '/videos/' + videoId + '.mp4',
      uploader: uploader,
      uploaderUsername: username,
      uploadDate: new Date(uploadDate),
      videoId: videoId,

      comments,
      related
    })

  } catch(err) {
    next(err);
  }
})

app.post('/comment/:videoId', async (req, res, next) => {
  try {
    const { comment } = req.body;
    const { videoId } = req.params;

    if (!req.session.userId) {
      res.redirect('/signin')

    } else {
      await db.run(`
        insert into comments (video, user, text)
               values (?, ?, ?);`, [videoId, req.session.userId, comment]);

      res.redirect('/watch/' + videoId)
    }

  } catch(err) {
    next(err)
  }
})

app.get('/channel/:channelId', async (req, res, next) => {
  try {
    const { channelId } = req.params;

    const videos = await db.all(`
      select videos.id, title, uploadDate
        from videos
        where uploader = ?;`,
      [channelId]);

    const { username } = await db.get(`select username from users where id=?;`, [channelId]);

    res.render('channel', {
      title: username + "'s channel",
      channel: { username, videos }
    })

  } catch(err) {
    next(err);
  }
})

Promise.resolve()
  .then(() => db.open('./database.sqlite', { Promise }))
  .then(() => db.migrate({}))
  .then(async () => {
    await db.each(`select * from videos;`, (err, res) => {
      index.add(res.id, res.title)
    })
  })
  .catch((err) => console.error(err.stack))
  .finally(() => { app.listen(port); console.log('listening') });
