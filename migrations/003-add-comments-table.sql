-- Up
create table comments(
  user text not null,
  video text not null,
  text text not null,

  foreign key(user) references users(id),
  foreign key(video) references video(id)
);

-- Down
drop table comments;
