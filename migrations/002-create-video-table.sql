-- Up
create table videos(
  id text primary key not null unique,
  title text not null,
  uploader text not null,
  uploadDate text not null,

  foreign key(uploader) references users(id));

-- Down
drop table videos;
