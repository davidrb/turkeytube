-- Up

create table users(
  id text primary key not null unique,
  username text unique,
  email text unique,
  passwordHash text not null
);

-- Down

drop table users;
