TurkeyTube 🥃
==========

## About
TurkeyTube is youtube, but for alcoholics. It is being developed by team Wild Turkey for ICS 491.

## Assignment 2 - Work Done So Far
- David Badke has set up a node project, using
  - ExpressJS for handling web requests
  - hbs for html templating

- David Herman is doing back-end development/database programming.
  His current task is setting up the sqlite module for node.

- Dennis Kim is doing front-end development in HTML/CSS.
  His current task is creating an html layout for each page to use.

- David Badke is doing full-stack development, and helping the rest of
  the team learn web development.

## Assignment 3 - Work Done So Far 
 - David Badke created and set up a user signup/login.
   His current task is to use the HTML/CSS and database to finish creating
   the web app.

 - Dennis Kim is doing the attack surface review.
   His current task is to help with the testings. 

 - David Herman is doing the dynamic analysis of the report. 
   His current task is to do the testings.

## Assignment 4 - Work Done So Far
 - David Badke added more features to the site, such as:
   - Video ploads
   - User comments on videos
   - Suggested videos
   
 - Dennis Kim is helping with fuzz testing.
   His current task is to create an incident response plan.   

 - David Herman is doing the reviews on the static and dynamic analysis.
   His current task is to do a final security review.

## Assignment 5 - Work Done
 - David Badke too much. 
 - Dennis Kim created an incident response plan.
 - David Herman did a final security review.

## Technical Notes
- To install, install ffmpeg and run 'yarn install' in the directory with the
  package.json file.
- To run, run the command 'node index.js'
